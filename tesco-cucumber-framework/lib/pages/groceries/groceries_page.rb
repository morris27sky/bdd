class GroceriesPage < BasePage

  set_url "/groceries"
  set_url_matcher %r{/groceries$}

  #Search elements

  element :search_for_multi_items_at_once, '[title="Search for multiple items at once"]'
  element :find_items_btn, '#searchNotepad .submit.button.npFind-2'
  element :find_items_btn_disabled, '#searchNotepad .submit.button.npFindDisabled'
  element :shopping_list_form, "#searchNotepad [id*='notepad']"
  element :clear_shopping_list_link, '#searchNotepad .npClear'
  element :search_input_form, '[name="searchBox"]'
  element :search_btn, '#searchBtn'
  element :header_content, '.headerContent h1'
  element :display_item_list, '#listItems .display ul li.currSearch a:first-child'
  element :edit_list_btn, '#listItems #editListFromLhs .button'
  element :edit_search_textarea, '#lhsEditable textarea'
  element :edit_search_searchBtn, '#lhsEditable .multisearchButtons .npFind-2'
  element :results_not_matched, '#searchResultsDidNotFind p'


  class Products < SitePrism::Section
    element :image, 'img'
    element :title, 'span[data-title="true"]'
    element :price, '.linePrice'
    element :add_to_basket, '.productListForm'
  end

  section :products_section, Products, '.allProducts .product'

  def search_multi_items
    self.search_for_multi_items_at_once.click
  end

  def add_shopping_list (query)
    self.shopping_list_form.set query  + "\n"
    find_item
  end

  def find_item
    self.find_items_btn.click
  end

  def verify_left_multi_search_container(query)
    expect(self.display_item_list.text.eql?(query)).to be_true, "Expected to see '#{query}' but got '#{self.display_item_list.text}'"
  end

  def verify_search_results_on_page(query)
    expect(self.header_content.text.equal "results for '#{query}'").to be_true
  end

  def clear_shopping_list
    self.clear_shopping_list_link.click
  end

  def verify_search_button_disabled
    expect(self.find_items_btn_disabled.visible?).to be_true
  end

  def edit_shopping_list(query)
    self.edit_list_btn.click
    self.edit_search_textarea.set query
    self.edit_search_searchBtn.click
  end

  def verify_product(query)
    expect(self.products_section.has_image?).to be_true
    expect(self.products_section.title.text.equal(query)).to be_true
    expect(self.products_section.has_price?).to be_true
    expect(self.products_section.has_add_to_basket?).to be_true
  end

  def verify_product_match_message
    message = "We didn't find any products to match 'spugetti' but we found the following products for 'spaghetti'"
    expect(self.results_not_matched.text.eql?(message)).to be_true
  end
end
