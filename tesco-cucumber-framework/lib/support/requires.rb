# External requires
require 'rspec'
require 'rspec/expectations'
require 'capybara/cucumber'
require 'capybara/rspec'
require 'selenium-webdriver'
require 'site_prism'
require 'json'
require 'capybara-screenshot/cucumber'
require 'active_support/core_ext/string'
require 'pry' if Gem::Specification::find_all_by_name('pry').any?
require_relative 'app'
require 'pages'
