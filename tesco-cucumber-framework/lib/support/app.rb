# App class, representing the app being tested

require 'ostruct'
$app_pages = Hash.new

class App
  attr_accessor :store

  def self.method_missing(method_sym)
    if not $app_pages[method_sym]
      $app_pages[method_sym] = Object::const_get(method_sym.to_s.camelize).new
    end

    $app_pages[method_sym]
  end

  # @return [Struct]
  def self.store
    @store ||= OpenStruct.new
  end

  def self.debug
    ENV['DEBUG']
  end

  # Set the environment
  def self.set_environment environment = ENV['ENV']
    case environment
      when 'prod'
        ENV['BASE_URL'] = "http://www.tesco.com"
    end
    Capybara.app_host = ENV['BASE_URL']
  end

  def self.get_environment
    ENV['ENV']
  end

  def self.get_browser
    ENV['BROWSER']
  end

  # --- Static methods below this point

  # @return [GroceriesPage]
  def self.groceries_page
    self.method_missing("groceries_page")
  end

end
