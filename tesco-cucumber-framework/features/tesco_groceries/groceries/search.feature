@tesco_search
Feature: Tesco groceries search functionality
  As a User
  I want to have be able to perform various searches on the both the top search form and the search by list 
  So that I may be able to find various items

  Background:
    Given I navigate to the groceries page

  Scenario: Test that a user can add an item to the "search with a list of items" form, perform a search and the search query should be displayed on the search page
    And I search for multiple items
    When I add 'Milk' to the shopping list and search
    Then I should see the search query for 'Milk' in the left multi search container
    And the search results on the page should contain 'Milk'

  Scenario: Search button should be disabled when shopping list is cleared
    And I search for multiple items
    And I add 'Bread' to the shopping list and search
    When I clear my my shopping list
    Then the search button should be disabled

  Scenario: Test that a user can edit a shopping list
    And I search for multiple items
    And I add 'Eggs' to the shopping list and search
    And I should see the search query for 'Eggs' in the left multi search container
    And the search results on the page should contain 'Eggs'
    When I edit my shopping list to 'Fish' and search
    Then I should see the search query for 'Fish' in the left multi search container
    And the search results on the page should contain 'Fish'

  Scenario: Test that a user can perform a single search and verify the products
    When I search for 'Tesco Granny Smith Apple Min 5 Pack 670G' on the top search form
    Then the query should return a list that contains the 'Tesco Granny Smith Apple Min 5 Pack 670G' with an image, desc, price and add to basket form

  Scenario: Test that when a user enters an item with the wrong spelling, the system displays a message with the suggetsed search item
    When I search for 'spugetti' on the top search form
    Then I should see a product not found warning message
