# Hooks to be executed before/after scenarios

Before do
  # Setup App environment and browser
  App.set_environment
  Capybara.page.driver.browser.manage.window.maximize
  page.driver.browser.manage.window.resize_to(1400, 1100)
end

Before '@chrome' do
  App.store.browser ||= App.get_browser
  App.store.environment ||= App.get_environment
  App.set_browser 'chrome'
end
