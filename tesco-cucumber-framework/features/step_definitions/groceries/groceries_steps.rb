Then(/^I add '(.*?)' to the shopping list and search$/) do |query|
  App.groceries_page.add_shopping_list(query)
end

Then(/^I search for multiple items$/) do
  App.groceries_page.search_multi_items
end

Then(/^I should see the search query for '(.*?)' in the left multi search container$/) do |query|
  App.groceries_page.verify_left_multi_search_container(query)
end

Then(/^the search results on the page should contain '(.*?)'$/) do |query|
  App.groceries_page.verify_search_results_on_page(query)
end

Then(/^I clear my my shopping list$/) do
  App.groceries_page.search_multi_items
  App.groceries_page.clear_shopping_list
end

Then(/^the search button should be disabled$/) do
  App.groceries_page.verify_search_button_disabled
end

Then(/^I edit my shopping list to '(.*?)' and search$/) do |query|
  App.groceries_page.edit_shopping_list(query)
end

Then(/^I search for '(.*?)' on the top search form$/) do |query|
  App.groceries_page.search_form(query)
end

Then(/^the query should return a list that contains the '(.*?)' with an image, desc, price and add to basket form$/) do |query|
  App.groceries_page.verify_product(query)
end

Then(/^I should see a product not found warning message$/) do
  App.groceries_page.verify_product_match_message
end
