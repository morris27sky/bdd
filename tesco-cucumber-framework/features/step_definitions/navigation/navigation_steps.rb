# --- steps ---

When(/^I navigate to the (.* page)$/) do |page|
  App.send(page.downcase.split(' ').join('_').to_sym).load
end
