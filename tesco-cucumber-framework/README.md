# Pre-req
Install RVM using one step curl from http://rvm.io/
Run `rvm install ruby-2.0.0` (to install Ruby 2.0.0)

Tested with Ruby 2.2.1p85

# Setup

Requires Bundler (`gem install bundler`)

1. cd tesco-cucumber-framework
2. bundle install

# Execution

Rake
To find a list of all available tasks from the root of the project execute the following command:

`rake -T`

This will display the name of the rake task and a brief description.

Example:

`rake features:default` # Runs tests using the 'default' cucumber profile (rake features:specific ENV=prod TAG=@tesco_search)

Options:

- ENV is prod i.e. ENV=prod
- BROWSER is chrome i.e. BROWSER=chrome

- If you wish to run a set of tests with a specific tag use:

`rake features:specific TAG=@tag`

You can also use rake to execute tests and other test related tasks or bundler

Bundler
bundle exec cucumber
