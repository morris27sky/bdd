require 'net/http'

#setup config for browser
class Driver
  
  DRIVER ||= ENV['DRIVER'] || 'prod'

  def self.setup	
    Capybara.configure do |config|
        config.default_driver = :selenium_chrome
        config.save_and_open_page_path = File.expand_path(File.join(Dir.pwd, "screenshots"))
    end    
    send(DRIVER) 
  end

  def self.prod
    caps = Selenium::WebDriver::Remote::Capabilities.chrome("chromeOptions" => {"args" => ["--disable-popup-blocking", "--disable-translate", "--start-maximized", "--test-type"]})
    Capybara.register_driver :selenium_chrome do |app|
      Capybara::Selenium::Driver.new(app, :browser => :chrome, :desired_capabilities => caps)
    end
    Capybara::Screenshot.register_driver(:selenium_chrome) do |driver, path|
      driver.save_screenshot(path)
    end
    Capybara.default_driver = :selenium_chrome
    Capybara.javascript_driver = :selenium_chrome
  end

  def default_wait_time=(t)
    self.default_max_wait_time = t
  end
end
