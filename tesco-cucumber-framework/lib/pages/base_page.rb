include RSpec::Matchers

class BasePage < SitePrism::Page
  set_url_matcher /ENV['BASE_URL']\/?/

  def search_form(query)
    self.search_input_form.set(query)
    self.search_btn.click
  end
end
