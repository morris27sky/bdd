require 'rake'
require 'rake/task'
require 'cucumber'
require 'cucumber/rake/task'
require 'yard'
require 'yaml'
require 'pry'

ENV['HTML_DIR'] ||= File.dirname(__FILE__) + '/html_report/'

desc 'Create HTML report folders'
task :create_dirs do
  Dir.mkdir(ENV['HTML_DIR']) unless Dir.glob('**/*/').include?('html_report/')
  Dir.mkdir('screenshots/') unless Dir.glob('**/*/').include?('screenshots/')
end

desc 'Delete old html reports and failed screenshots locally'
task :clean_report_dir do
  FileUtils.rm_r Dir.glob(ENV['HTML_DIR'] + '*.html')
  FileUtils.rm_r Dir.glob(File.dirname(__FILE__) +'/screenshots/' + '*.png')
  FileUtils.rm_r Dir.glob(File.dirname(__FILE__) +'/screenshots/' + '*.html')
end

namespace :features do
  # Iterate through the cucumber profiles and create a rake task for each profile
  profiles = YAML::load(File.open(File.dirname(__FILE__) + '/cucumber.yml'))

  profiles.each_key do |profile|
    Cucumber::Rake::Task.new(profile.to_sym, "Runs tests using the '#{profile}' cucumber profile") do |t|
      t.profile = profile
      t.cucumber_opts = "--format json -o #{File.dirname(__FILE__)}/screenshots/cucumber_report.json --format html -o #{ENV['HTML_DIR']}results.html -f pretty"
      t.libs << 'lib'
    end
  end
end
